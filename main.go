package main

import (
	"fmt"
	"github.com/tealeg/xlsx"
	"strconv"
	"strings"
)

type Data struct {
	id int
	grade string

}

func dataSruct(file string) []*Data{
	data := []*Data{}
	xlFile, err := xlsx.OpenFile(file)
	if err != nil {
		fmt.Println(err)
	}
	for _, sheet := range xlFile.Sheets {
		for _, row := range sheet.Rows {
			dat := new(Data)
			for i, cell := range row.Cells {
				switch i {
					case 0:
						text, _ := cell.Int()
						dat.id = text
						break
					default:
						text := cell.String()
						dat.grade = text
				}
			}
			data = append(data, dat)
		}
	}

	return data
}

func main(){
	file := "D:/Project/go/src/Evalia/xtrail.xlsx"
	hasil := []*Data{}
	data := dataSruct(file)

	var mekar *xlsx.File
	var sheet *xlsx.Sheet
	var row *xlsx.Row
	var cell *xlsx.Cell
	var err error

	for x := range data {
		d := data[x]
		if d.id > 0 {
			if strings.Contains(d.grade,"+")  {
				//kodeSplit := strings.Split(d.kode,"+")
				gradeSplit := strings.Split(d.grade,"+")
				//transSplit := strings.Split(d.trans,"+")
				for i := range gradeSplit {
					h := new(Data)
					h.id = d.id
					h.grade = gradeSplit[i]
					//h.kode = kodeSplit[i]
					//h.trans = transSplit[i]
					hasil = append(hasil,h)
				}
			}else {
				hasil = append(hasil,d)
			}

		}

	}

	mekar = xlsx.NewFile()
	sheet, err = mekar.AddSheet("Sheet1")
	if err != nil {
		fmt.Printf(err.Error())
	}
	for y := range hasil {
		row = sheet.AddRow()
		cell = row.AddCell()
		cell.Value = strconv.Itoa(hasil[y].id)
		fmt.Println(strconv.Itoa(hasil[y].id))
		//cell = row.AddCell()
		//cell.Value = hasil[y].kode
		cell = row.AddCell()
		cell.Value = hasil[y].grade
		//cell = row.AddCell()
		//cell.Value = hasil[y].trans
	}
	err = mekar.Save("hasil_xtrail.xlsx")
	if err != nil {
		fmt.Printf(err.Error())
	}
}